# meige-driver

#### 介绍
M美格驱动拨号工具

#### 软件架构
软件架构说明


#### 安装教程

MeiG-QMI放到 openwrt/package/wwan/
meig-cm放到 openwrt/package/app/

make menuconfig
勾选kmod-qmi-wwan-meig
在Kernel modules /WWAN Support
然后勾选meig-cm
在Utilities /meig-cm

最好是第一次配置，不然可能会与其它插件冲突

要支持RNDIS,请勾选.,并支持MBIM
kmod-usb-net-rndis
kmod-usb-net-cdc-ether
并编译美格MEIG-CM拨号后使用以下命令:

udhcpc –i usb0 –s /etc/udhcpc/default.script


其它
usbutils
pciutils
sms-tool
jq

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
